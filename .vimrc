"let &t_SI = "\<Esc>]50;CursorShape=1\x7"
"let &t_SR = "\<Esc>]50;CursorShape=2\x7"
"let &t_EI = "\<Esc>]50;CursorShape=0\x7"

set go +=!
set guifont=SFMonoNerdFontComplete-Semibold:h18
set ttimeout
set wildmenu
set ttimeoutlen=1
set ttyfast
set nu
set mh
set tgc
set et
set ic
set noswf
set hls
set is
set ar
set clipboard=unnamed
set mouse=a
set sw=4
set ts=4
filetype indent on
set ai
set si
let g:auto_save = 1


nmap <S-h> :bprev <cr>
nmap <S-l> :bnext <cr>
nmap <S-d> :bdelete <cr>
autocmd filetype python nmap <C-r> :!python3 main.py < in.txt <cr>
autocmd filetype go nmap <C-r> :!go run % < in.txt <cr>
autocmd filetype cpp nmap <C-r> :!g++ -std=c++17 -fsanitize=address -fsanitize=undefined -Wall -Wextra -Wshadow -Dchasm -o %< % && ./%< < inp.txt > out.txt && cat inp.txt && echo -e "\e[1;32m$(cat out.txt)\e[0m"<cr>

nmap <C-c> ggVGy <cr>
nmap <C-a> ggVG <cr>

call plug#begin()
Plug 'nanotech/jellybeans.vim'
Plug '907th/vim-auto-save'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'plan9-for-vimspace/acme-colors'
call plug#end()
colorscheme acme

set viewoptions-=options
augroup remember_folds
    autocmd!
    autocmd BufWinLeave *.* if &ft !=# 'help' | mkview | endif
    autocmd BufWinEnter *.* if &ft !=# 'help' | silent! loadview | endif
augroup END


set updatetime=300
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

nmap <silent> [d <Plug>(coc-diagnostic-prev)
nmap <silent> ]d <Plug>(coc-diagnostic-next)

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

autocmd CursorHold * silent call CocActionAsync('highlight')

nmap <leader>rn <Plug>(coc-rename)
