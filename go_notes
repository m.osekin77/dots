// Slices
# we create slice from an existing array. 
# we can use 2 elements of the slice from created base array
# but we can't access rest of them, even though the cap(s) = 5
# because our current slice points to the memory of the base array.
a := [...]int{3, 1, 4, 2, 7, 8}
s := a[1:3]
fmt.Println(len(s), cap(s))

# uninitialized slice is nil
var list []int
fmt.Println(list == nil) // true
# but if we add composite literal initialazation
list = []int{}
fmt.Println(list == nil) // false
# check if slice is empty with len(slice) == 0

# two line are equivalent
var x []string = []string(nil) // explicit
var x []string  // non-explicit

# if we can, allocate the memory beforehand to avoid redunadnt copying
a := []int{1,2,3}
func double(target []int) []int {
    res := make([]int, 0, cap(target)) 
    for _, x := range target {
        res = append(res, x * 2)
    }
    return res
}

# element indexes(keys) are optional in arrays in and slices
[4]bool{false, true, true, false}
# is equivalent to
[4]bool{0: false, 1: true, 2: true, 3: false}

# an element which index(key) is abscent uses previous available index + 1
a := [...]int{1: 5, 3: 6, 7} // [0, 5, 0, 6, 7], len(5)
fmt.Println(a)

# even though it seems like we pass a copy, but in fact we pass pointer to the base array of the slice
func double(target []int, val int) {
    for i, _ := range target {
        target[i] = val
    }
}
a := []int{1, 2, 3}
double(a, 5) // [5, 5, 5]


# when we len(slice) == cap(slice), copying and reallocating the base array
list := make([]int, 4, 4)
list2 := append(list, 1)
list[0] = 10
list2[0] = 20
fmt.Println(list, list2) // [10 0 0 0] [20 0 0 0 1], different base arrays
# but when len(slice) != cap(slice), reallocation will not happen
list := make([]int, 4, 5)
list2 := append(list, 1)
list[0] = 10
list2[0] = 20 // also changes the first slice
fmt.Println(list, list2) // [20 0 0 0] [20 0 0 0 1]

# if we want to create a copy of the slice in order not to modify the underlying slice/array
buf := []int{1, 2, 3, 4, 5}
n := append([]int{}, buf...) // create a copy
//or 
n := make([]int, len(buf)) // another way
copy(n, buf)

// Strings
# Strings in go are immutable by default, each character is represented by 1 byte
# If you want to change it, we can convert it to the mutable slice of bytes
s := "artem"
// s[0] = 'x' - can't
m_s := []byte(s)
m_s[0] = 'x'
fmt.Println(string(m_s))

# we can escape \, \x, \u into the strings
\ - followed by 3 digits to represent an octal values "\141\142\143" - abc
\x - followed by 2 digits to represent a hex value "x61\x62\x63" - abc
\u - unicode character  "\xe4\xbc\x97\xe4\xba\xba" = "众人"  

"" - are interpreted strings, which will convert esaped sequences to the characters, but `` - are raw, and \ are treated like simple characters.

// Constants
# Declaration order of package level constants does not matter, BUT THE SAME CODE WON'T COMPILE IF IT WAS DECLARED LOCALLY
Const (
    No = !Yes
    Yes = false
)

1.Value literals must be able to be represented by typed constant
# INVALID 
const a uint8 = uint8(255) + 1

2.Even though int can fit bigger values, types should match
# INVALID
const a int = uint8(255) - 1

3.Since int is platform specific, some code might not compile on 32bit OS
const a uint = (1 << 64) - 1,
instead use: 
const a uint = ^uint(0)

4.iota can only be used in constant declarations and is local to every new constant decalration
const (
    z = 0
    a = 1 << iota // 2
    b // 4 
    c // 8
    d // 16
)

# Variable declarations
# If we specify a type, then all variables must be of the same type
var x, y int = 1, 2
# But compiler can deduce it, or we can use equivalent short decalrations
var golang, isDynamic = "GO", false

goland, isDynamic := "GO", false

# Operators
^x - bitwise not (~ in c++)
x &^ - bitwise clear (1101 & 0101 = 1000) - clear all bits on left operand where there are set bits on the right one.

# Type conversions
const (
        X  = 2
        Y  = 'A'
)

const (
        XX int = 2
        YY rune = 'A'
)

a := X + Y // rune
b := XX + YY // undefined
c := X + YY // rune
d := X + XX // int

# Functions
# Even though we have declared the parameters, we may not use them, and this will compile just fine.
func squares(a, b int64) (int64, int64) {
    return 2 * 3, 4 * 5
}
squares(1000, 1<<25)

# There is no default parameters, the initial value of each result is the zero value of its type
func f() (x int, y bool) {
    x *= 5
    return x, y
}
f() // 0, false

# When we pass parameters to the function, we should think of that as an ASSIGNMENT OPERATION
# Because of that, parameters that we pass, simply should be CONVERTABLE to the type of the parameters int the function.

func f(x, y int) (res int) {
    res = x + y
    return res
}
f(1.0, 2.0) // no truncation, OK COMPILES
f(1.5, 2.0) // truncation, BAD DOES NOT COMPILE

# Exiting phase of a function
A function call may undergo an EXITING PHASE (IF IT HAS DEFERRED CALLS PUSHED TO THE FUNCTION STACK)
# there are 3 ways function can enter exiting phase:
    1. function worked normally, reached return
    2. panic occured
    3. runtime.Goexit() was called, note: call to this can not be recovered
func f0() int {
    var x = 1
    defer fmt.Println("normal exit")
    x++
    return x
}


func f2() {
    var x, y = 1, 0
    defer fmt.Println("exits because of panic)
    x = x / y
    x++ // unreachable code
}

func f3() int {
    x := 1
    defer fmt.Println("exits for goexiting")
    x++
    runtime.Goexit() // don't call it directly
    return x + x // unreachable
}

# Deferred function calls can modify named returned values

func f(n int) int {
    var res int
    defer func(){
        res += n
    }()
    res += n
    return res
}

func y(n int) (res int) {
    defer func(){
        res += n
    }()
    return n + n
}

# Even though we declared a separate visibility zone, it still is going to change the outer variables
x, y := func()(int, int){
    return 3, 4
}()
fmt.Println(x, y) // 3, 4

{
    x, y = 5, 5
    fmt.Println(x, y) // 5, 5
}

fmt.Println(x, y) // 5, 5

// Map
# If key does not exist, it will return the default value for the type
mp := map[string]int{"a":1}
val := mp["b"]
fmt.Println(val) // 0 - beceause of int

// Type safety
# In Go type safety if a crucial part. Even if compound types have the same underlying type
# they are still treated as DIFFERENT types

type UserID int

func solve(io *IO) {
    idx := 1
    kek := UserID(1)
    fmt.Println(kek == idx) // type mismatch
}

// Lambda functoins
job := func(x int) (res int) {
    res = x * 2
    fmt.Println("x,", x)
    fmt.Print("res,", res, "\n")
    return res
}(5)
fmt.Print(job)


// Defer calls
# deferred calls are executed AFTER the surrounding function returns
func incX(x *int) *int {
    *x++
    return x
}

func solve(io *IO) {
    x := 5
    f := func() int {
        defer incX(&x)
        return x
    }
    fmt.Println(f())
    fmt.Print(x)
}

// panic() and recover()
# sometimes unexpected stuff might happen which can lead to the runtime error
# (division by zero, index out of range, dereferncing nill pointer etc...)
# in this case we can catch that, and continue execution of our programm without full crash
# and the function will not return any value, which means in case of painc, it is executed BEFORE
func f() (res int) {
    defer func(){
        if panic_err := recover(); panic_err != nil {
            fmt.Println("caught panic,", panic_err)
        }
    }()
    
    a, x := 5, 0
    res = a / x
    fmt.Println("return will not happen!")
    return res
}

func solve(io *IO) {
    f()
    fmt.Println("continue execution...")
}
# but still keep in mind, that if there is no error, defer is invoked AFTER the function returns
we can only recover panic, if the deferred function in the same stack as caller function
package main
ex1:
func main() {
	func() {
		defer func() {
			if v := recover(); v != nil {
				fmt.Println("recovered panic")
			}
		}()
	}()
	panic("a") // unrecoverable panic, because recover is in another function stack.
}
ex2:
func demo() {
	defer func() {
		defer func() {
			if v := recover(); v != nil {
				fmt.Printf("recoverd %v\n", v)
			}
		}()
		panic(2) // can recover this
	}()
	panic(1) // nothing recovers this panic
}

func main() {
	demo()
}

# Statements
1.Simple:
    - short variable declaration
    - pure value assignments (including x op=y )
    - function calls, channel recieve and send
    - nothing
    - increment and decrement
2.All other are compound 


# Gouroutines
GREEN THREADS - threads, that are manager by the language runtime environment. The cost and memory consumption and context switching is much less compared to the OS threads.

# SCHEDULING
The standard Go runtime adopts the M-P-G model 🗗  to do the goroutine schedule job, where M represents OS threads, P represents logical/virtual processors (not logical CPUs) and G represents goroutines. Most schedule work is made by logical processors (Ps), which act as brokers by attaching goroutines (Gs) to OS threads (Ms). Each OS thread can only be attached to at most one goroutine at any given time, and each goroutine can only be attached to at most one OS thread at any given time. A goroutine can only get executed when it is attached to an OS thread. A goroutine which has been executed for a while will try to detach itself from the corresponding OS thread, so that other running goroutines can have a chance to get attached and executed. 

# blocking and non blocking operations
blocking operation - waits for some condition to be satisfied before proceeding
non-blocking operation - does not wait and proceeds anyway

ex of blocking op:
    ch := make(chan int)
    go func(){
        _ = <-ch
    }()
    ch <- 5 // blocking

ex of non blocking operation
    ch := make(chan int, 1)
    ch <- 5 
    _ = <-ch // will procede

    ch := make(chan int, 1)
    select x := <-ch{
        fmt.Println(x)
    }

### intresting examples 
# 1
ch := make(chan int)
wg := &sync.WaitGroup{}
ctx, cancel := context.WithCancel(context.Background())

shutdown := make(chan os.Signal, 1)
signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

wg.Add(1)
go func() {
    defer wg.Done()
    for {
        select {
        case <-ctx.Done():
            close(ch)
            return
        default:
            x := rand.Intn(10) + 1
            ch <- x
            time.Sleep(1 * time.Second)
        }
    }
}()

wg.Add(1)
go func() {
    defer wg.Done()
Loop:
    for {
        select {
        case val, ok := <-ch:
            if ok {
                fmt.Println(val, ok)
                time.Sleep(1 * time.Second)
            } else {
                fmt.Println("try to recieve on closed channel", val, ok)
                break Loop
            }
        }
    }
}()
<-shutdown
fmt.Println("call cancel()")
cancel()
wg.Wait()

# 2 There is no value in the channel, so it won't be read at all.
c := make(chan int)
select {
case val, ok := <-c:
    if ok {
        fmt.Println(val, ok)
}
default:
    fmt.Println("chan is empty")
    break
}

# Types and Structs
# types can be comparable and incomparable:
    incomparable are: slices and maps
    every other type is comparable

# type can be converted if one type can be represented as other types

import "fmt"

type A struct {
	x int
}
type B struct {
	x int
}

func main() {
	a := A{x: 5}
	b := B{x: 5}
	fmt.Println(a == A(b)) // true
}

# order of field declaration matter in structs.
# two structs are identical, if order of their fields is the same
# and the corresponding fields have the same type

s1 := struct {
    x int
    y int
}{}
s2 := struct {
    y int
    x int
}{}

# addresing the value field of the struct is called SELECTOR or 
SELECTOR EXPRESSIONS (v.x, v.y)

# assigning one struct to the other is the same as assigning each of
their corresponding fields

# in field selectors dereference of recievers can be implicit
type Book struct {
    pages int
}
b1 := &Book{100}
b2 := new(Book)

b2.pages = b1.pages
// equivalent
(*b2).pages = (*b1).pages

fmt.Println(b1, b2)

# Map
# keys for the map type should be of the comparable type
mp := make(map[[3]int]int)
var a [3]int = [3]int{1,2,3}
mp[a] = 1
fmt.Println(mp[a]) // OK, 1

# deleting of unexisting keys in map will not cause any painc, even if map is nil
var nilMap map[int]string
delete(nilMap, 5)
fmt.Println(nilMap)


# Container assignments
If a map is assigned to another map, then the two maps will share all (underlying) elements. Appending elements into (or deleting elements from) one map will reflect on the other map. Like map assignments, if a slice is assigned to another slice, they will share all (underlying) elements. Their respective lengths and capacities equal to each other. However, if the length/capacity of one slice changes later, the change will not reflect on the other slice. When an array is assigned to another array, all the elements are copied from the source one to the destination one. The two arrays don't share any elements.

# Methods in GO
Compiler automatically generated a method for *Human,but stil we can not change the value.

import "fmt"

type Human struct {
    age int
}

func (h Human) SetAge(x int) {
    h.age = x
}

func NewHumanPtr() *Human {
    return &Human{
        age: 0,
    }
}

func main() {
    ptr := NewHumanPtr()
    ptr.SetAge(10)
    fmt.Println(ptr)
}

# functions which return values can not be deferred unless wrapped in lambda
s := []string{"a", "b", "c", "d"}
// defer append(s[:1], "x", "y", "z") wrong
defer fmt.Println(s)
defer func(){
    _ = append(s[:1], "x", "y", "z")
}()

// value of deferred function calls are evaluated when pushed to the deferred call queue
f := func() {
    fmt.Println(false)
}
defer f() // false
f = func() {
    fmt.Println(true)
}

//when calling a nil function in deferred call a panic will occur
var f func()
defer f() // panic

// deferred function calls for reciever arguments will be evaluated and only the last one will be deferred
package main

type T int

func (t T) M(n int) T {
	println(n)
	return t
}

func main() {
	var t T
	defer t.M(1).M(2).M(3)
	t.M(4).M(5)
}
// 1 2 4 5 3


# Evaluation order
# Evaluation order outside of containers, is guaranteed to be in lexicographical order
from left to right
y[z.f()], ok = g(h(a, b), i()+x[j()], <-c), k()
z.f() -> h() -> i() -> j() -> <-c -> g() -> k()
h evaluated after a and b
y evaluated after z.f()
z.f() afer z
x[] after j()

# but order is not guaranteed in containers
a := 1
f := func() {
    a++
    return a
}
s := []int{a, f()}

// m can be {2:1}, {1:2}
m := map[int]int{a: 1, a: 2}
m := map[int]int{a: f()}

c := make(chan int, 1)
c <- 0

fchan := func(info string, c chan int) chan int {
    fmt.Println(info)
    return c
}
fptr := func(info string) *int {
    fmt.Println(info)
    return new(int)
}

select {
case *fptr("aaa") = <-fchan("bbb", nil): // fptr will never be evaluated
case *fptr("ccc") = <-fchan("ddd", c): // fchan evaluated first, then fptr
}
// bbb
// ddd
// ccc

# Value copy costs
a := [1000]int{1,2,3,4,5}
b := []int{1,2,3}
fmt.Println(unsafe.Sizeof(a)) // Type * num_elements = 8 * 1000 = 8000
fmt.Println(unsafe.Sizeof(b)) // sizeof direct part
// pointer to the array + len + cap = sizeof(int) + int + int = 24

# Optimizations?
We should try to avoid using the two-iteration-variable form to iterate array and slice elements
if the element types are large-size types, for each element will be copied to the second variable.
